package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.IOrderRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderController {

	@Autowired
	IOrderRepository iOrderRepository;

	@GetMapping("/orders")
	public ResponseEntity<List<Order>> getAllOrder() {
		try {
			List<Order> pOrder = new ArrayList<Order>();
			iOrderRepository.findAll().forEach(pOrder::add);
			return new ResponseEntity<>(pOrder, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/orders/{id}")
	public ResponseEntity<Order> getOrderById(@PathVariable("id") Integer id) {
		Optional<Order> OrderData = iOrderRepository.findById(id);
		if (OrderData.isPresent()) {
			return new ResponseEntity<>(OrderData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/orders")
	public ResponseEntity<Object> createOrder(@RequestBody Order pOrder) {//Sửa để cho phép validate
		try {
			Order _Order = iOrderRepository.save(pOrder);
			return new ResponseEntity<>(_Order, HttpStatus.CREATED);
		}catch (Exception  e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			//return ResponseEntity.unprocessableEntity().body("Failed to Create specified Order: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/orders/{id}")
	public ResponseEntity<Object> updateOrderById(@PathVariable("id") Integer id, @RequestBody Order pOrder) {
		Optional<Order> OrderData = iOrderRepository.findById(id);
		if (OrderData.isPresent()) {
			Order Order= OrderData.get();
			Order.setComments(pOrder.getComments());
			Order.setOrderDate(pOrder.getOrderDate());
			Order.setRequiredDate(pOrder.getRequiredDate());
			Order.setShippedDate(pOrder.getShippedDate());
			Order.setStatus(pOrder.getStatus());
			try {
				return new ResponseEntity<>(iOrderRepository.save(Order), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Order:"+e.getCause().getCause().getMessage());
			}
			
		} else {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Order: "+id + "  for update.");
		}
	}

	@DeleteMapping("/orders/{id}")
	public ResponseEntity<Order> deleteOrderById(@PathVariable("id") Integer id) {
		try {
			iOrderRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/orders")
	public ResponseEntity<Order> deleteAllOrder() {
		try {
			iOrderRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
