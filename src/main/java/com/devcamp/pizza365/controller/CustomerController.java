package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.ICustomerRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerController {

	@Autowired
	ICustomerRepository iCustomerRepository;

	@GetMapping("/customers")
	public ResponseEntity<List<Customer>> getAllCustomer() {
		try {
			List<Customer> pCustomer = new ArrayList<Customer>();

			iCustomerRepository.findAll().forEach(pCustomer::add);

			return new ResponseEntity<>(pCustomer, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/customers/{id}")
	public ResponseEntity<Customer> getCustomerById(@PathVariable("id") Integer id) {
		Optional<Customer> customerData = iCustomerRepository.findById(id);
		if (customerData.isPresent()) {
			return new ResponseEntity<>(customerData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/customers")
	public ResponseEntity<Object> createCustomer(@RequestBody Customer pCustomer) {//Sửa để cho phép validate
		try {
			Customer _Customer = iCustomerRepository.save(pCustomer);
			return new ResponseEntity<>(_Customer, HttpStatus.CREATED);
		}catch (Exception  e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			//return ResponseEntity.unprocessableEntity().body("Failed to Create specified customer: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/customers/{id}")
	public ResponseEntity<Object> updateCustomerById(@PathVariable("id") Integer id, @RequestBody Customer pCustomer) {
		Optional<Customer> customerData = iCustomerRepository.findById(id);
		if (customerData.isPresent()) {
			Customer customer= customerData.get();
			customer.setAddress(pCustomer.getAddress());
			customer.setCity(pCustomer.getCity());
			customer.setCountry(pCustomer.getCountry());
			customer.setCreditLimit(pCustomer.getCreditLimit());
			customer.setFirstName(pCustomer.getFirstName());
			customer.setLastName(pCustomer.getLastName());
			customer.setPhoneNumber(pCustomer.getPhoneNumber());
			customer.setPostalCode(pCustomer.getPostalCode());
			customer.setSalesRepEmployeeNumber(pCustomer.getSalesRepEmployeeNumber());
			try {
				return new ResponseEntity<>(iCustomerRepository.save(customer), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified customer:"+e.getCause().getCause().getMessage());
			}
			
		} else {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified customer: "+id + "  for update.");
		}
	}

	@DeleteMapping("/customers/{id}")
	public ResponseEntity<Customer> deleteCustomerById(@PathVariable("id") Integer id) {
		try {
			iCustomerRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/customers")
	public ResponseEntity<Customer> deleteAllCustomer() {
		try {
			iCustomerRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
