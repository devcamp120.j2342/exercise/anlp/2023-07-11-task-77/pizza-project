package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.repository.IProductRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductControllers {

	@Autowired
	IProductRepository iProductRepository;

	@GetMapping("/products")
	public ResponseEntity<List<Product>> getAllProduct() {
		try {
			List<Product> pProduct = new ArrayList<Product>();

			iProductRepository.findAll().forEach(pProduct::add);

			return new ResponseEntity<>(pProduct, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/products/{id}")
	public ResponseEntity<Product> getProductById(@PathVariable("id") Integer id) {
		Optional<Product> ProductData = iProductRepository.findById(id);
		if (ProductData.isPresent()) {
			return new ResponseEntity<>(ProductData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/products")
	public ResponseEntity<Object> createProduct(@RequestBody Product pProduct) {//Sửa để cho phép validate
		try {
			Product _Product = iProductRepository.save(pProduct);
			return new ResponseEntity<>(_Product, HttpStatus.CREATED);
		}catch (Exception  e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			//return ResponseEntity.unprocessableEntity().body("Failed to Create specified Product: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/products/{id}")
	public ResponseEntity<Object> updateProductById(@PathVariable("id") Integer id, @RequestBody Product pProduct) {
		Optional<Product> ProductData = iProductRepository.findById(id);
		if (ProductData.isPresent()) {
			Product Product= ProductData.get();
			Product.setProductCode(pProduct.getProductCode());
			Product.setProductName(pProduct.getProductName());
			Product.setProductDescripttion(pProduct.getProductDescripttion());
			Product.setIdProductLine(pProduct.getIdProductLine());
			Product.setProductScale(pProduct.getProductScale());
			Product.setProductVendor(pProduct.getProductVendor());
			Product.setQuantityInStock(pProduct.getQuantityInStock());
			Product.setBuyPrice(pProduct.getBuyPrice());
		try {
				return new ResponseEntity<>(iProductRepository.save(Product), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Product:"+e.getCause().getCause().getMessage());
			}
			
		} else {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Product: "+id + "  for update.");
		}
	}

	@DeleteMapping("/products/{id}")
	public ResponseEntity<Product> deleteProductById(@PathVariable("id") Integer id) {
		try {
			iProductRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/products")
	public ResponseEntity<Product> deleteAllProduct() {
		try {
			iProductRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
