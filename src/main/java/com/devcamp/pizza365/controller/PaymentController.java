package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.repository.IPaymentRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class PaymentController {

	@Autowired
	IPaymentRepository iPaymentRepository;

	@GetMapping("/payments")
	public ResponseEntity<List<Payment>> getAllPayment() {
		try {
			List<Payment> pPayment = new ArrayList<Payment>();

			iPaymentRepository.findAll().forEach(pPayment::add);

			return new ResponseEntity<>(pPayment, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/payments/{id}")
	public ResponseEntity<Payment> getPaymentById(@PathVariable("id") Integer id) {
		Optional<Payment> PaymentData = iPaymentRepository.findById(id);
		if (PaymentData.isPresent()) {
			return new ResponseEntity<>(PaymentData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/payments")
	public ResponseEntity<Object> createPayment(@RequestBody Payment pPayment) {//Sửa để cho phép validate
		try {
			Payment _Payment = iPaymentRepository.save(pPayment);
			return new ResponseEntity<>(_Payment, HttpStatus.CREATED);
		}catch (Exception  e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			//return ResponseEntity.unprocessableEntity().body("Failed to Create specified Payment: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/payments/{id}")
	public ResponseEntity<Object> updatePaymentById(@PathVariable("id") Integer id, @RequestBody Payment pPayment) {
		Optional<Payment> PaymentData = iPaymentRepository.findById(id);
		if (PaymentData.isPresent()) {
			Payment Payment= PaymentData.get();
			Payment.setCustomer(pPayment.getCustomer());
			Payment.setCheckNumber(pPayment.getCheckNumber());
			Payment.setPaymentDate(pPayment.getPaymentDate());
			Payment.setAmmount(pPayment.getAmmount());
			try {
				return new ResponseEntity<>(iPaymentRepository.save(Payment), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Payment:"+e.getCause().getCause().getMessage());
			}
			
		} else {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Payment: "+id + "  for update.");
		}
	}

	@DeleteMapping("/payments/{id}")
	public ResponseEntity<Payment> deletePaymentById(@PathVariable("id") Integer id) {
		try {
			iPaymentRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/payments")
	public ResponseEntity<Payment> deleteAllPayment() {
		try {
			iPaymentRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
