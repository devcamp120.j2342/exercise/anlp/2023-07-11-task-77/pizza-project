package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.repository.IOrderDetailRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderDetailController {

	@Autowired
	IOrderDetailRepository iOrderDetailRepository;

	@GetMapping("/orderDetails")
	public ResponseEntity<List<OrderDetail>> getAllOrderDetail() {
		try {
			List<OrderDetail> pOrderDetail = new ArrayList<OrderDetail>();
			iOrderDetailRepository.findAll().forEach(pOrderDetail::add);
			return new ResponseEntity<>(pOrderDetail, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

	@GetMapping("/orderDetails/{id}")
	public ResponseEntity<OrderDetail> getOrderDetailById(@PathVariable("id") Integer id) {
		Optional<OrderDetail> OrderDetailData = iOrderDetailRepository.findById(id);
		if (OrderDetailData.isPresent()) {
			return new ResponseEntity<>(OrderDetailData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/orderDetails")
	public ResponseEntity<Object> createOrderDetail(@RequestBody OrderDetail pOrderDetail) {//Sửa để cho phép validate
		try {
			OrderDetail _OrderDetail = iOrderDetailRepository.save(pOrderDetail);
			return new ResponseEntity<>(_OrderDetail, HttpStatus.CREATED);
		}catch (Exception  e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			//return ResponseEntity.unprocessableEntity().body("Failed to Create specified OrderDetail: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/orderDetails/{id}")
	public ResponseEntity<Object> updateOrderDetailById(@PathVariable("id") Integer id, @RequestBody OrderDetail pOrderDetail) {
		Optional<OrderDetail> OrderDetailData = iOrderDetailRepository.findById(id);
		if (OrderDetailData.isPresent()) {
			OrderDetail OrderDetail= OrderDetailData.get();
			OrderDetail.setOrder(pOrderDetail.getOrder());
			OrderDetail.setProduct(pOrderDetail.getProduct());
			OrderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
			OrderDetail.setPriceEach(pOrderDetail.getPriceEach());
			try {
				return new ResponseEntity<>(iOrderDetailRepository.save(OrderDetail), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified OrderDetail:"+e.getCause().getCause().getMessage());
			}
			
		} else {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified OrderDetail: "+id + "  for update.");
		}
	}

	@DeleteMapping("/orderDetails/{id}")
	public ResponseEntity<OrderDetail> deleteOrderDetailById(@PathVariable("id") Integer id) {
		try {
			iOrderDetailRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/orderDetails")
	public ResponseEntity<OrderDetail> deleteAllOrderDetail() {
		try {
			iOrderDetailRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
