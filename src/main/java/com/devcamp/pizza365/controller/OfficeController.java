package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.repository.IOfficeRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OfficeController {

	@Autowired
	IOfficeRepository iOfficeRepository;

	@GetMapping("/offices")
	public ResponseEntity<List<Office>> getAllOffice() {
		try {
			List<Office> pOffice = new ArrayList<Office>();

			iOfficeRepository.findAll().forEach(pOffice::add);

			return new ResponseEntity<>(pOffice, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/offices/{id}")
	public ResponseEntity<Office> getOfficeById(@PathVariable("id") Integer id) {
		Optional<Office> OfficeData = iOfficeRepository.findById(id);
		if (OfficeData.isPresent()) {
			return new ResponseEntity<>(OfficeData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/offices")
	public ResponseEntity<Object> createOffice(@RequestBody Office pOffice) {//Sửa để cho phép validate
		try {
			Office _Office = iOfficeRepository.save(pOffice);
			return new ResponseEntity<>(_Office, HttpStatus.CREATED);
		}catch (Exception  e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			//return ResponseEntity.unprocessableEntity().body("Failed to Create specified Office: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/offices/{id}")
	public ResponseEntity<Object> updateOfficeById(@PathVariable("id") Integer id, @RequestBody Office pOffice) {
		Optional<Office> OfficeData = iOfficeRepository.findById(id);
		if (OfficeData.isPresent()) {
			Office Office= OfficeData.get();
			Office.setCity(pOffice.getCity());
			Office.setPhone(pOffice.getPhone());
			Office.setAddressLine(pOffice.getAddressLine());
			Office.setState(pOffice.getState());
			Office.setCountry(pOffice.getCountry());
			Office.setTerritory(pOffice.getTerritory());
			try {
				return new ResponseEntity<>(iOfficeRepository.save(Office), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Office:"+e.getCause().getCause().getMessage());
			}
			
		} else {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Office: "+id + "  for update.");
		}
	}

	@DeleteMapping("/offices/{id}")
	public ResponseEntity<Office> deleteOfficeById(@PathVariable("id") Integer id) {
		try {
			iOfficeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/offices")
	public ResponseEntity<Office> deleteAllOffice() {
		try {
			iOfficeRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
