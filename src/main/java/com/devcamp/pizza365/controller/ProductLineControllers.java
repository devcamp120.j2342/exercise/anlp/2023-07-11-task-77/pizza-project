package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductLineControllers {

	@Autowired
	IProductLineRepository iProductLineRepository;

	@GetMapping("/productLines")
	public ResponseEntity<List<ProductLine>> getAllProductLine() {
		try {
			List<ProductLine> pProductLine = new ArrayList<ProductLine>();

			iProductLineRepository.findAll().forEach(pProductLine::add);

			return new ResponseEntity<>(pProductLine, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/productLines/{id}")
	public ResponseEntity<ProductLine> getProductLineById(@PathVariable("id") Integer id) {
		Optional<ProductLine> ProductLineData = iProductLineRepository.findById(id);
		if (ProductLineData.isPresent()) {
			return new ResponseEntity<>(ProductLineData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/productLines")
	public ResponseEntity<Object> createProductLine(@RequestBody ProductLine pProductLine) {//Sửa để cho phép validate
		try {
			ProductLine _ProductLine = iProductLineRepository.save(pProductLine);
			return new ResponseEntity<>(_ProductLine, HttpStatus.CREATED);
		}catch (Exception  e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			//return ResponseEntity.unprocessableEntity().body("Failed to Create specified ProductLine: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/productLines/{id}")
	public ResponseEntity<Object> updateProductLineById(@PathVariable("id") Integer id, @RequestBody ProductLine pProductLine) {
		Optional<ProductLine> ProductLineData = iProductLineRepository.findById(id);
		if (ProductLineData.isPresent()) {
			ProductLine ProductLine= ProductLineData.get();
			ProductLine.setProductLine(pProductLine.getProductLine());
			ProductLine.setDescription(pProductLine.getDescription());
		try {
				return new ResponseEntity<>(iProductLineRepository.save(ProductLine), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified ProductLine:"+e.getCause().getCause().getMessage());
			}
			
		} else {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified ProductLine: "+id + "  for update.");
		}
	}

	@DeleteMapping("/productLines/{id}")
	public ResponseEntity<ProductLine> deleteProductLineById(@PathVariable("id") Integer id) {
		try {
			iProductLineRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/productLines")
	public ResponseEntity<ProductLine> deleteAllProductLine() {
		try {
			iProductLineRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
