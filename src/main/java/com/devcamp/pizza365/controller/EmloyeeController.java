package com.devcamp.pizza365.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.repository.IEmployeeRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class EmloyeeController {

	@Autowired
	IEmployeeRepository iEmployeeRepository;

	@GetMapping("/employees")
	public ResponseEntity<List<Employee>> getAllEmployee() {
		try {
			List<Employee> pEmployee = new ArrayList<Employee>();

			iEmployeeRepository.findAll().forEach(pEmployee::add);

			return new ResponseEntity<>(pEmployee, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/employees/{id}")
	public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Integer id) {
		Optional<Employee> EmployeeData = iEmployeeRepository.findById(id);
		if (EmployeeData.isPresent()) {
			return new ResponseEntity<>(EmployeeData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/employees")
	public ResponseEntity<Object> createEmployee(@RequestBody Employee pEmployee) {//Sửa để cho phép validate
		try {
			Employee _Employee = iEmployeeRepository.save(pEmployee);
			return new ResponseEntity<>(_Employee, HttpStatus.CREATED);
		}catch (Exception  e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			//return ResponseEntity.unprocessableEntity().body("Failed to Create specified Employee: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/employees/{id}")
	public ResponseEntity<Object> updateEmployeeById(@PathVariable("id") Integer id, @RequestBody Employee pEmployee) {
		Optional<Employee> EmployeeData = iEmployeeRepository.findById(id);
		if (EmployeeData.isPresent()) {
			Employee Employee= EmployeeData.get();
			Employee.setFirstName(pEmployee.getFirstName());
			Employee.setLastName(pEmployee.getLastName());
			Employee.setExtension(pEmployee.getExtension());
			Employee.setEmail(pEmployee.getEmail());
			Employee.setOfficeCode(pEmployee.getOfficeCode());
			Employee.setReportTo(pEmployee.getReportTo());
			Employee.setJobTitle(pEmployee.getJobTitle());
			try {
				return new ResponseEntity<>(iEmployeeRepository.save(Employee), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Employee:"+e.getCause().getCause().getMessage());
			}
			
		} else {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Employee: "+id + "  for update.");
		}
	}

	@DeleteMapping("/employees/{id}")
	public ResponseEntity<Employee> deleteEmployeeById(@PathVariable("id") Integer id) {
		try {
			iEmployeeRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/employees")
	public ResponseEntity<Employee> deleteAllEmployee() {
		try {
			iEmployeeRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
